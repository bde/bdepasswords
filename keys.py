#!/usr/bin/env python
# -*- encoding: utf-8 -*-

KEYS = {
    u'pollion': (u'bombar@crans.org', u'C352C156DCCDFD64CFD6577B84EAFB7218C74ED1'),
    u'arcas' : (u'sellem@crans.org', u'A3F59C7DB33F84B7ED80C618E260E370F7D0B7BA'),
    u'gaboutch' : (u'lebouder@crans.org', u'3AB1E89D9C93DA9D3F1B3CD38A3C83BFE34C3154'),
    u'grizzly'  : (u'grisel-davy@crans.org', u'9323E2F7DB4359C13FB03A50A363CFF0718109DC'),
    u'peb' : (u'becue@crans.org', u'9AE04D986400E3B67528F4930D442664194974E2'),
    u'vincent' : (u'legallic@crans.org', u'1645DC4855278B0357E713A7D5759143B14B45B6'),
    u'oursdur' : (u'antoine.bernard@crans.org', u'2B940D9AD5773088F81DFE37E9C87A31E9AC89C0'),
    u'fardale' : (u'arrighi@crans.org', u'82046BC3922B7229D4C7807C45D62605B1302D3E'),
}
