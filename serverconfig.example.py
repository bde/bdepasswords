#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Configuration Serveur de bdepasswords.

Sont définis ici les utilisateurs et les rôles associés.
"""

#: Pour override le nom si vous voulez renommer la commande
cmd_name = 'bdepasswords'

#: Chemin vers la commande sendmail
sendmail_cmd = '/usr/lib/sendmail'

#: Répertoire de stockage des mots de passe
STORE = '/var/lib/%s/db/' % (cmd_name,)

#: Ce serveur est-il read-only (on ne peut pas y modifier les mots de passe)
READONLY = False

#: Expéditeur du mail de notification
CRANSP_MAIL = u"%s <respo-info.bde@lists.crans.org>" % (cmd_name,)

#: Destinataire du mail de notification
DEST_MAIL = u"respo-info.bde@lists.crans.org"

#: Mapping des utilisateurs et de leurs (mail, fingerprint GPG)
KEYS = {
    u'mikachu': (u'paulon@crans.org', u'FD6D9F6052B4AF5BFFDEBEB38BC97BC8087051A2'),
    u'pollion': (u'bombar@crans.org', u'C352C156DCCDFD64CFD6577B84EAFB7218C74ED1'),
}
#: Les variables suivantes sont utilisées pour définir le dictionnaire des
#: rôles.

#: Liste des usernames des respo-info
RI = [
    u'pollion'
    u'mikachu'
    u'arcas'
    ]

#: Liste des usernames des vieux respo-info
VIEUX = []

#: Les roles utilisés pour savoir qui a le droit le lire/écrire quoi
ROLES = {
    "ri": RI,
    "ri-w": RI,
    "vieux" : VIEUX,
    "vieux-w" : VIEUX
}
