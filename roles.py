#!/usr/bin/env python
# -*- encoding: utf-8 -*-


#: Admin de bdepasswords, a tous les droits.
ADMIN = [u'pollion',]


BDE2018 = [u'grizzly',]

BDE2017 = [u'arcas',
           u'pollion',
           ]

BDE2016 = [u'gaboutch',]

BDE2015 = [u'oursdur',
           u'fardale',
           ]

BDE2011 = [u'vincent',
           u'peb',
           ]

#: Liste des usernames des respo-info
RI = BDE2011 + BDE2015 + BDE2016 + BDE2017 + BDE2018

#: Le bureau du bde
BUREAU = []

#: Les Trez
TREZ = []

#: Les Screz
SCREZ = []

#: L'intersection Gala/bde est très fortement non vide
GALA = [
    u'pollion',
    ]

#: Les roles utilisés pour savoir qui a le droit le lire/écrire quoi
ROLES = {
    "bde2017": list(set(BDE2017 + ADMIN)),
    "bde2017-w": list(set(BDE2017 + ADMIN)),
    "ri": list(set(RI + ADMIN)),
    "ri-w": list(set(RI + ADMIN)),
    "trez" : list(set(TREZ + ADMIN)),
    "trez-w" : list(set(TREZ + ADMIN)),
    "screz" : list(set(SCREZ + ADMIN)),
    "screz-w" : list(set(SCREZ + ADMIN)),
    "bureau" : list(set(BUREAU + ADMIN)),
    "bureau-w" : list(set(BUREAU + ADMIN)),
    "gala" : list(set(GALA + ADMIN)),
    "gala-w" : list(set(GALA + ADMIN)),
    }
