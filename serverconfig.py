#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Configuration Serveur de bdepasswords.

Sont définis ici les utilisateurs et les rôles associés.
"""

#: Pour override le nom si vous voulez renommer la commande
cmd_name = 'bdepasswords'

#: Chemin vers la commande sendmail
sendmail_cmd = '/usr/lib/sendmail'

#: Répertoire de stockage des mots de passe
STORE = '/var/lib/%s/db/' % (cmd_name,)

#: Ce serveur est-il read-only (on ne peut pas y modifier les mots de passe)
READONLY = False

#: Expéditeur du mail de notification
CRANSP_MAIL = u"%s <respo-info.bde@lists.crans.org>" % (cmd_name,)

#: Destinataire du mail de notification
DEST_MAIL = u"respo-info.bde@lists.crans.org"

#: Mapping des utilisateurs et de leurs (mail, fingerprint GPG)
from keys import KEYS
from roles import ROLES
